#!/bin/bash
silkpath=`cat silkajpath.txt`
peer=`./choosepeer.sh`

# Number of addresses
nbdests=`grep -c "" addresses.txt`

echo "Requesting amounts on peer $peer..."
for i in `seq $nbdests`
do
	# Get the full line for this address
	line=`awk '{if(NR==n)print $0'} n=$i addresses.txt`
	# Retrieve the address itself and remove the double quotes around it
	newaddr=`echo $line|awk '{print $2}'|sed 's/"//g'`
	if [ "$addr" == "" ]
	then
		# This is the first address - just take it
		addr=$newaddr
	else
		# This is a subsequent address - add the colon separator
		addr=$addr:$newaddr
	fi
done
# Ask silkaj all the amounts
# Filter only the lines containing totals
# Group them by 3 and print only the address and the total
$silkpath amount $addr -p $peer|grep Total|awk 'ORS=(NR%3)?" ":"\n"'|awk '{print NR " " $4 " = " $14}'

