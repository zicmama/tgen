#!/bin/bash
peer=`./choosepeer.sh`
dte=`date "+%Y-%m-%d/%H:%M:%S"`
echo "$dte Sending $2 units from $1 to $3 on peer $peer..."
silkpath=`cat silkajpath.txt`
srcseed=`awk '{if(NR==n)print $3}' n=$1 addresses.txt|sed 's/"//g'`
srcaddr=`awk '{if(NR==n)print $2}' n=$1 addresses.txt|sed 's/"//g'`
destaddr=`awk '{if(NR==n)print $2}' n=$3 addresses.txt|sed 's/"//g'`
uglysep="this is the separator"

result=$(
	{ stdout=$(echo $srcseed|$silkpath tx --amount=$2 --output $destaddr --auth-seed --yes -p $peer); returncode=$?; } 2>&1
	printf "$uglysep"
	printf "%s\n" "$stdout"
	exit "$returncode"
)
returncode=$?
var_out=${result#*$uglysep}
var_err=${result%$uglysep*}

if [ $returncode == 0 ]
then
	echo "$dte $srcaddr $2 $destaddr" >>success.log
	echo "$dte $var_out" >>debug.log
else
	notenough=`echo $var_out|grep "enough money"`
	if [ "$notenough" == "" ]
	then
		echo "$dte" >>error.log
		echo "$var_err" >>error.log
		echo "$dte $peer" >>peer_error.log
	else
		echo "$dte $srcaddr $2" >>not_enough.log
	fi
fi

